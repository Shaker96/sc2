const logoutBtn = document.getElementById('logout')

const popup = document.createElement('div')
popup.className = 'logout-popup'
popup.id = 'logout-popup'

const popupText = document.createTextNode('Log-out')
popupText.className = 'text'

popup.appendChild(popupText)

document.addEventListener('click', function (event) {
    const isClickInside = popup.contains(event.target) || logoutBtn.contains(event.target);

    if (!isClickInside) {
        popup.remove()
    }
});

logoutBtn.addEventListener('click', function (event) {
    this.parentElement.appendChild(popup)
})
