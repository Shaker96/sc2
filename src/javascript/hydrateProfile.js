/**
 * fills provided data in respective inputs 'profile' or 'content' depending on 'set' variable
 * 
 * set === 'true' for labels of 'about' view
 * 
 * @param {object} data 
 * @param {boolean} set 
 */
export default function hydrateProfile(data, set = false) {
    let settingsId = set ? 'set-' : '';
    const keys = Object.keys(data)
    keys.map((key) => {
        let elem = document.getElementById(settingsId + key)
        if(elem)
            elem.innerHTML = data[key] 
    })
}