// require('file-loader?name=[name].[ext]!../../index.html');

import '../sass/styles.scss'

import profileData from './profileData.json'
import navBar from './utils/navBar'
import tooltip from './utils/tooltip.js'
import hydrateProfile from './hydrateProfile.js'
import { createPopup, closePopup }  from './Popup.js'
import getAllInputs from './getAllInputs.js'
import getProfileDataValues from './getProfileDataValues.js'
import './logoutPopup.js'

import aboutComponent from '../html/about.html'

import profilePicture from '../images/profile_image.jpg'

export {
    hydrateProfile,
    createPopup,
    closePopup,
    getAllInputs,
    getProfileDataValues,
}

const profilePictureDiv = document.querySelector('.profile__picture')
const profilePictureimg = document.createElement('img')
profilePictureimg.src = profilePicture
profilePictureDiv.appendChild(profilePictureimg)
 
hydrateProfile(profileData) // initial profile hydratation

const components = {
    'about': aboutComponent,
    'settings':'<div class="content__tab"><h2>Settings</h2></div>',
    'option1': '<div class="content__tab"><h2>Option 1</h2></div>',
    'option2': '<div class="content__tab"><h2>Option 2</h2></div>',
    'option3': '<div class="content__tab"><h2>Option 3</h2></div>',
    'option4': '<div class="content__tab"><h2>Option 4</h2></div>',
    'option5': '<div class="content__tab"><h2>Option 5</h2></div>',
    'option6': '<div class="content__tab"><h2>Option 6</h2></div>',
    'option7': '<div class="content__tab"><h2>Option 7</h2></div>',
}

const newNavBar = new navBar(components)
newNavBar.create()

const newTooltip = new tooltip('followers', 'Follow')

window.addEventListener('click', () => {
    const about = document.getElementById('about-page')
    if (about) {
        hydrateProfile(profileData, true)
    }
})