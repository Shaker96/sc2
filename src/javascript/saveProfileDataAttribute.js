import hydrateProfile from './hydrateProfile.js'
import profileData from './profileData.json'

/**
 * Saves 'value' into profileData json 'key' attribute  
 * 
 * @param {string} key 
 * @param {string} value 
 */
export default function saveProfileDataAttribute(key, value) {
    profileData[key] = value
    hydrateProfile(profileData)
    hydrateProfile(profileData, true)
}