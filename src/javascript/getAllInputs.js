import editPage from '../html/edit.html'
import profileData from './profileData.json'

const labelsMobile = {
    name: "FIRST NAME",
    lastname: "LAST NAME",
    website: "WEBSITE",
    phone: "PHONE NUMBER",
    address: "CITY, STATE & ZIP"
}


/**
 * Fills all input fields in the edit view of the about page in mobile
 */
export default function getAllInputs() {
    document.getElementById('content-container').innerHTML = String(editPage)
    const keys = Object.keys(profileData)
    keys.map((key) => {
        if (key === 'name'){
            let split = profileData[key].split(' ')
            document.getElementById('edit-label-name').innerHTML = labelsMobile.name
            document.getElementById('edit-input-name').value = split[0]
            document.getElementById('edit-label-lastname').innerHTML = labelsMobile.lastname
            document.getElementById('edit-input-lastname').value = split[1]
        } else {
            let label = document.getElementById('edit-label-' + key)
            let input = document.getElementById('edit-input-' + key)
            if (label && input){
                label.innerHTML = labelsMobile[key]
                input.value = profileData[key]
            }
        }
    })
}