import threeDotsImg from '../../images/three-dots.png'

export default class navBar {
    constructor(items) {
        this.items = items
        this.navBarElem = document.getElementById('custom-nav-bar')

        this.dotsMenu = document.createElement('div')
        this.dotsMenu.className = 'nav__dots-menu'
        this.dotsMenu.id = 'dots-menu'

        this.dots = document.createElement('div')
        this.dots.className = 'nav__item'

        this.toggleBoundFunction = this.toggleDotMenu.bind(this)
        
        const imgElem = document.createElement('img')
        imgElem.src = threeDotsImg
        this.dots.appendChild(imgElem)

        this.createBoundFunction = this.create.bind(this);
        window.addEventListener("resize", this.createBoundFunction);

        // window.removeEventListener("scroll", this.scrollBoundFunction);
    }

    setContent(component) {
        const contentDiv = document.getElementById("content-container")
        contentDiv.innerHTML = component
    }

    toggleDotMenu() {
        console.log('toggledotmenu', this)
        if (this.navBarElem.querySelector('.nav__dots-menu')) {
            this.navBarElem.removeChild(this.dotsMenu)
        } else {
            this.navBarElem.appendChild(this.dotsMenu)
        }
    }

    create() {

        this.navBarElem.innerHTML = ''
        this.dotsMenu.innerHTML = ''

        let dotFlag = false;

        Object.keys(this.items).forEach((key) => {

            this.navBarElem.appendChild(this.dots)
            this.dots.addEventListener("click", this.toggleBoundFunction)

            const currentHeight = this.navBarElem.scrollHeight
            
            const newItem = document.createElement('div')
            newItem.innerHTML = key.toUpperCase()
            newItem.addEventListener('click', () => { this.setContent(this.items[key]) })
            newItem.className = 'nav__item'
            this.navBarElem.appendChild(newItem)

            if (this.navBarElem.scrollHeight > currentHeight && currentHeight !== 0) {
                dotFlag = true
            }

            if (dotFlag) {
                this.navBarElem.removeChild(newItem)
                this.dotsMenu.appendChild(newItem)
            } else {
                this.navBarElem.removeChild(this.dots)
                this.dots.removeEventListener("click", this.toggleBoundFunction)
            }
        })

    }

    
}