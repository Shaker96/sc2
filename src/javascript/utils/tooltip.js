export default class tooltip {
    constructor(parentId, text) {
        this.parent = document.getElementById(parentId)
        // this.parent.style.position = 'relative';

        this.tooltip = document.createElement('div')
        this.tooltip.className = 'tooltip'
        this.tooltip.id = 'tooltip'

        const tooltipText = document.createTextNode(text)
        tooltipText.className = 'text'

        this.tooltip.appendChild(tooltipText)

        this.parent.addEventListener('mouseenter', this.show.bind(this))

        this.parent.addEventListener('mouseleave', this.hide.bind(this))
    }

    show() {
        this.parent.appendChild(this.tooltip)
    }

    hide() {
        this.parent.removeChild(this.tooltip)
    }
}