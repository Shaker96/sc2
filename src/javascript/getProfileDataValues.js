import profileData from './profileData.json'
import saveProfileDataAttribute from './saveProfileDataAttribute.js'

/**
 * Retrieves the values from all inputs in mobile edit view, saves and updates the profile
 */
export default function getProfileDataValues() {
    const keys = Object.keys(profileData)
    keys.map((key) => {
        let value = null
        
        if (key === 'name') {
            value = document.getElementById('edit-input-' + 'name').value
            value += ' ' + document.getElementById('edit-input-' + 'lastname').value
        } else {
            let input = document.getElementById('edit-input-' + key)
            
            if (input) {
                value = input.value
            }
        }
        
        if (value !== null) {
            saveProfileDataAttribute(key, value)
            // setContent('about')
        }
    }) 
}