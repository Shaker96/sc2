import popupHtml from '../html/popup.html'
import profileData from './profileData.json'
import saveProfileDataAttribute from './saveProfileDataAttribute.js'

const labels = {
    name: "FULLNAME",
    website: "WEBSITE",
    phone: "PHONE NUMBER",
    address: "CITY, STATE & ZIP"
}

const popupComponent = String(popupHtml)

/**
 * Closes input popup
 */
export function closePopup() {
    document.getElementById('popup').remove()
}

/**
 * creates popup container to edit the input in profileData with 'key'
 * 
 * @param {html caller element} elem 
 * @param {string} key 
 */
export function createPopup(elem, key) {
    const currentPopup = document.getElementById('popup')
    if(currentPopup)
        currentPopup.remove()

    const parent = elem.parentElement    
    parent.innerHTML += popupComponent
    
    document.getElementById('edit-label').innerHTML = labels[key]
    document.getElementById('edit-input').value = profileData[key]
    
    const saveBtn = document.getElementById('edit-save');
    saveBtn.onclick = function (event) {
        const newValue = document.getElementById('edit-input').value
        saveProfileDataAttribute(key, newValue)
        closePopup()
    }
}

